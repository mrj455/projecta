package projecta.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;

import projecta.db.UserRepository;
import projecta.model.User;

public class UserControllerTest {

	@Test
	public void shouldShowRegistrationForm() throws Exception {
		UserRepository mockRepository = mock(UserRepository.class);
		UserController controller = new UserController(mockRepository);
		MockMvc mockMvc = standaloneSetup(controller).build();

		mockMvc.perform(get("/register")).andExpect(view().name("registerForm"));
	}

	@Test
	public void shouldProcessRegistration() throws Exception {
		UserRepository mockRepository = mock(UserRepository.class);
		User unsaved = new User("testuser", "user123", "testuser@email.com");
		User saved = new User("testuser", "user123", "testuser@email.com");

		when(mockRepository.addUser(unsaved)).thenReturn(saved);

		UserController controller = new UserController(mockRepository);
		MockMvc mockMvc = standaloneSetup(controller).build();

		mockMvc.perform(post("/register").param("username", "testuser").param("password", "user123").param("email",
				"testuser@email.com")).andExpect(redirectedUrl("/user/testuser"));

		verify(mockRepository, atLeastOnce()).addUser(unsaved);

	}

}
