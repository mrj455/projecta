package projecta.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceView;

import projecta.db.ProjectRepository;
import projecta.model.Project;

public class ProjectControllerTest {

	@Test
	public void shouldShowRecentProjects() throws Exception {
		List<Project> expectedProjects = createProjectList(5);

		ProjectRepository mockRepository = mock(ProjectRepository.class);
		when(mockRepository.getProjects(0, 5)).thenReturn(expectedProjects);

		ProjectController controller = new ProjectController(mockRepository);
		MockMvc mockMvc = standaloneSetup(controller)
				.setSingleView(new InternalResourceView("/WEB-INF/views/projects.jsp")).build();

		mockMvc.perform(get("/projects")).andExpect(view().name("projects"))
				.andExpect(model().attributeExists("projectList"))
				.andExpect(model().attribute("projectList", hasItems(expectedProjects.toArray())));

	}

	@Test
	public void shouldShowPagedProjects() throws Exception {
		List<Project> expectedProjects = createProjectList(10);

		ProjectRepository mockRepository = mock(ProjectRepository.class);
		when(mockRepository.getProjects(5, 10)).thenReturn(expectedProjects);

		ProjectController controller = new ProjectController(mockRepository);
		MockMvc mockMvc = standaloneSetup(controller)
				.setSingleView(new InternalResourceView("/WEB-INF/views/projects.jsp")).build();

		mockMvc.perform(get("/projects?start=5&count=10")).andExpect(view().name("projects"))
				.andExpect(model().attributeExists("projectList"))
				.andExpect(model().attribute("projectList", hasItems(expectedProjects.toArray())));

	}

	@Test
	public void shouldGetSingleProject() throws Exception {
		Project expectedProject = new Project(5, "Hello", "Test", Collections.emptyList());
		ProjectRepository mockRepository = mock(ProjectRepository.class);
		when(mockRepository.getProject(5)).thenReturn(expectedProject);

		ProjectController controller = new ProjectController(mockRepository);
		MockMvc mockMvc = standaloneSetup(controller).build();

		mockMvc.perform(get("/project/5")).andExpect(view().name("project"))
				.andExpect(model().attributeExists("project")).andExpect(model().attribute("project", expectedProject));
	}

	private List<Project> createProjectList(int count) {
		List<Project> projects = new ArrayList<Project>();
		for (int i = 0; i < count; i++) {
			projects.add(new Project(i, "Project " + i, "Test", null));
		}
		return projects;
	}

}
