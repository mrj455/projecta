
--testuser pwd = password
insert into users (username, password, email, description, enabled) values('testuser','$2a$10$Wn5vHF4TZBi0lRlcIWmiB.pMXKTf3kKK3rNsO8X2iX1aQz7ahI3aS','test@user.com','a test description for the test user !', true);

--mike pwd = mikej123
insert into users (username, password, email, enabled) values('mikej','$2a$10$AJQfb894Sgx6mHoMEcaCB.CJvJkxoLJSFzryCQ0kt/hinOBcri0Ky','mikej123@mikej123.com', true);

--admin pwd = admin
insert into users (username, password, email, enabled) values('admin','$2a$10$.kttWLEBje992jlYP8bgAeQZksIHAHF5l83eI1LWOF/3.oNbd/xAq','admin@admin.com', true); 

--admin pwd = admin

insert into authorities(username, authority) values('testuser','ROLE_USER');
insert into authorities(username, authority) values('mikej','ROLE_USER');
insert into authorities(username, authority) values('admin','ROLE_ADMIN');
insert into authorities(username, authority) values('admin','ROLE_USER');