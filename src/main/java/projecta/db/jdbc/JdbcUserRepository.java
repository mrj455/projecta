package projecta.db.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import projecta.db.UserRepository;
import projecta.model.User;

@Repository
public class JdbcUserRepository implements UserRepository {

	private static final String GET_USER_QUERY = "select userId, username, password, email, description, enabled from users where username=?";
	private static final String ADD_USER_QUERY = "insert into users (username, password, email, description, enabled) values (?, ?, ?, ?, ?)";
	private static final String ADD_USER_ROLE_QUERY = "insert into authorities(username, authority) values(?,'ROLE_USER')";
	private static final String USERNAME_IN_USE_QUERY = "select count(userId) from users where username = ?";

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcUserRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public User getUser(String username) {
		return jdbcTemplate.queryForObject(GET_USER_QUERY, new UserRowMapper(), username);
	}

	@Override
	public User addUser(User user) throws UsernameInUseException {

		if (usernameInUse(user.getUsername())) {
			throw new UsernameInUseException();
		}

		jdbcTemplate.update(ADD_USER_QUERY, user.getUsername(), user.getPassword(), user.getEmail(),
				user.getDescription(), true);

		jdbcTemplate.update(ADD_USER_ROLE_QUERY, user.getUsername());

		return getUser(user.getUsername());
	}

	private static final class UserRowMapper implements RowMapper<User> {
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			long id = rs.getLong("userId");
			String username = rs.getString("username");
			String password = rs.getString("password");
			String email = rs.getString("email");
			String description = rs.getString("description");
			Boolean enabled = rs.getBoolean("enabled");
			return new User(id, username, password, email, description, enabled);
		}
	}

	private boolean usernameInUse(String username) {

		boolean result = true;

		int count = jdbcTemplate.queryForObject(USERNAME_IN_USE_QUERY, new Object[] { username }, Integer.class);

		if (count == 0) {
			result = false;
		}

		return result;
	}

}
