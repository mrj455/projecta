package projecta.db.jdbc;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.I_AM_A_TEAPOT, reason="Username is already in use")
public class UsernameInUseException extends RuntimeException {

}
