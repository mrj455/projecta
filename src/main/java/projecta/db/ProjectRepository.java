package projecta.db;

import java.util.List;

import projecta.model.Project;

public interface ProjectRepository {
	
	List<Project> getProjects(int start, int count);
	
	Project getProject(long id);
	
	int getProjectCount();
	
	int addProject(Project project);
	
}
