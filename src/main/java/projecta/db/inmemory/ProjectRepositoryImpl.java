package projecta.db.inmemory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import projecta.db.ProjectRepository;
import projecta.model.Project;

@Component
public class ProjectRepositoryImpl implements ProjectRepository {

	private List<Project> projects = new LinkedList<>();

	public ProjectRepositoryImpl() {

		for (int i = 0; i < 10; i++) {
			List<String> t = new ArrayList<>();
			t.add("Test task for project " + i);
			Project p = new Project(2 + i, "Project" + i, "Project description goes here", t);
			projects.add(p);
		}

		List<String> t1 = new ArrayList<>();
		t1.add("Test task 123");
		t1.add("Another test task!!");
		Project p1 = new Project(0, "Project One!", "A Great Project", t1);
		projects.add(p1);

		List<String> t2 = new ArrayList<>();
		t2.add("abc");
		t2.add("123");
		t2.add("xyz");
		Project p2 = new Project(1, "to do list", "Stuff need to do", t2);
		projects.add(p2);

	}

	@Override
	public List<Project> getProjects(int start, int count) {

		List<Project> results = new ArrayList<>();
		int loadedCount = 0;

		if (projects.size() == 0) {
			return results;
		}

		for (int i = getProjectCount() - 1 - start; i >= 0 && loadedCount < count; i--) {
			results.add(projects.get(i));
			loadedCount++;
		}

		return results;
	}

	@Override
	public Project getProject(long id) {

		for (Project project : projects) {
			if (project.getId() == id) {
				return project;
			}
		}
		return null;
	}

	@Override
	public int getProjectCount() {
		return projects.size();
	}

	// TODO
	@Override
	public int addProject(Project project) {
		return -1;
	}

}
