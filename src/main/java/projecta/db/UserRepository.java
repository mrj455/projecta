package projecta.db;

import projecta.model.User;

public interface UserRepository {
	
	User getUser(String username);
	
	User addUser(User user);
	
	//<List>User getUsers(int start, int count) -> have user list start with the oldest user...
	
	//a method to edit users
	
	//a method to disable users

	//a methods to delete user
	
	//count for number of users
}
