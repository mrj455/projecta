package projecta.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import projecta.db.ProjectRepository;
import projecta.model.Project;

@Controller
public class ProjectController {

	private ProjectRepository projectRepository;

	@Autowired
	public ProjectController(ProjectRepository projectRepository) {
		this.projectRepository = projectRepository;
	}

	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public List<Project> projects(@RequestParam(value = "start", defaultValue = "0") int start,
			@RequestParam(value = "count", defaultValue = "5") int count) {

		return projectRepository.getProjects(start, count);
	}
	
	@RequestMapping(value="project/{projectId}", method=RequestMethod.GET)
	public String project(@PathVariable long projectId, Model model) {
		model.addAttribute(projectRepository.getProject(projectId));
		return "project";
	}

}
