package projecta.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import projecta.db.UserRepository;
import projecta.db.jdbc.UsernameInUseException;
import projecta.model.RegisterForm;
import projecta.model.User;

@Controller
public class UserController {

	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	public UserController(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showRegistrationForm(Model model) {
		model.addAttribute("registerForm", new RegisterForm());
		return "registerForm";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String processRegistration(@Valid RegisterForm registerForm, Errors errors, Model model) {

		// TODO add service layer for business logic!
		// UserService should have a User register(RegisterForm ) method

		if (errors.hasErrors()) {
			return "registerForm";
		}

		registerForm.setPassword(passwordEncoder.encode(registerForm.getPassword()));

		//TODO get rid of try catch
		try {
			userRepository.addUser(registerForm.toUser());
		} catch (UsernameInUseException e) {
			model.addAttribute("usernameTaken", e.getMessage());
			return "registerForm";
		}

		// TODO log in as user just created/ redirect login form (currently:
		// /user/* requires logged in, so redirects to login form)

		return "redirect:/user/" + registerForm.getUsername();
	}

	@RequestMapping(value = "/user/{username}", method = RequestMethod.GET)
	public String showUserProfile(@PathVariable String username, Model model) {
		User user = userRepository.getUser(username);
		model.addAttribute(user);
		return "profile";
	}

}
